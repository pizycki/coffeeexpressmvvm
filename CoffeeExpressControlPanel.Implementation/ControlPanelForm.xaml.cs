﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace CoffeeExpressControlPanel.Implementation
{
    /// <summary>
    /// Interaction logic for ControlPanelForm.xaml
    /// This class stands for View in MVVM model.
    /// </summary>
    public partial class ControlPanelForm : Window
    {
        public ControlPanelForm()
        {
            InitializeComponent();
        }

        public void OnPropertyChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == Properties.SelectedCoffeeType)
            {
                // Set all coffee buttons styles to default
                ResetBtnsStyles();
            }
        }

        private void ResetBtnsStyles()
        {
            EspressoBtn.Style = Resources["CommonUnactiveControlButtonStyle"] as Style;
            MokkaBtn.Style = Resources["CommonUnactiveControlButtonStyle"] as Style;
            LatteBtn.Style = Resources["CommonUnactiveControlButtonStyle"] as Style;
            CappuccinoBtn.Style = Resources["CommonUnactiveControlButtonStyle"] as Style;
        }

        private void CoffeeButton_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button)
            {
                ResetBtnsStyles();
                Button btn = sender as Button;
                btn.Style = Resources["CommonActiveControlButtonStyle"] as Style;
            }
        }

        public class Properties
        {
            public const string SelectedCoffeeType = "SelectedCoffeeType";
        }
    }
}
