﻿using System.Windows.Input;
using System.ComponentModel;

using CoffeeExpressControlPanel.Contract;

namespace CoffeeExpressControlPanel.Implementation
{
    /// <summary>
    /// Represents ViewModel class in MVVM model.
    /// NOTE: In our case the model is simple string variable and there is no sense for creating a new class for one variable.
    /// </summary>
    public class ControlPanelViewModel : INotifyPropertyChanged
    {
        private string _selectedCoffeeType;
        private ICoffeeExpressControlPanel _controlPanel;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        public ICommand EspressoCmd { get; set; }
        public ICommand LatteCmd { get; set; }
        public ICommand MokkaCmd { get; set; }
        public ICommand CappuccinoCmd { get; set; }
        public ICommand StartCmd { get; set; }

        public string SelectedCoffeeType
        {
            get { return this._selectedCoffeeType; }
            private set { this._selectedCoffeeType = value; }
        }

        #endregion

        public ControlPanelViewModel(ICoffeeExpressControlPanel controlPanel)
        {
            _controlPanel = controlPanel;

            // Bind commands
            this.EspressoCmd = new RelayCommand(parms => OnEspressoClick());
            this.LatteCmd = new RelayCommand(parms => OnLatteClick());
            this.MokkaCmd = new RelayCommand(parms => OnMokkaClick());
            this.CappuccinoCmd = new RelayCommand(parms => OnCappuccinoClick());
            this.StartCmd = new RelayCommand(parms => Start());
        }

        #region Button handlers

        void OnEspressoClick()
        {
            SelectCoffee(CoffeeTypes.Espresso);
        }
        void OnMokkaClick()
        {
            SelectCoffee(CoffeeTypes.Mokka);
        }
        void OnCappuccinoClick()
        {
            SelectCoffee(CoffeeTypes.Cappuccino);
        }
        void OnLatteClick()
        {
            SelectCoffee(CoffeeTypes.Latte);
        }

        #endregion

        public void SelectCoffee(string coffeeType)
        {
            this.SelectedCoffeeType = coffeeType;
        }

        public void Start()
        {
            _controlPanel.Start(this.SelectedCoffeeType);
        }

        /// <summary>
        /// Reset state of ViewModel.
        /// </summary>
        public void Reset()
        {
            SelectedCoffeeType = null;

            // update GUI
            OnPropertyChanged(ControlPanelForm.Properties.SelectedCoffeeType);
        }

        public void OnPropertyChanged(string propName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propName));
        }
    }

    public static class CoffeeTypes
    {
        public const string Mokka = "Mokka";
        public const string Espresso = "Espresso";
        public const string Cappuccino = "Cappuccino";
        public const string Latte = "Latte";
    }
}
