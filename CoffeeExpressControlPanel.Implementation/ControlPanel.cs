﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using CoffeeExpressMainController.Contract;
using CoffeeExpressControlPanel.Contract;


namespace CoffeeExpressControlPanel.Implementation
{
    /// <summary>
    /// Main class of ControlPanel. 
    /// </summary>
    public class ControlPanel : ICoffeeExpressControlPanel
    {
        ControlPanelViewModel _modelView;
        ControlPanelForm _view;
        IMainController _mainController;

        /// <summary>
        /// Window with control panel.
        /// </summary>
        public Window ControlPanelWindow
        {
            get { return _view; }
        }

        public ControlPanel(IMainController mainController)
        {
            _mainController = mainController;

            _modelView = new ControlPanelViewModel(this);
            _view = new ControlPanelForm();
            _view.DataContext = _modelView;

            // Subscribe for listening of property changes
            _modelView.PropertyChanged += _view.OnPropertyChangedHandler;
        }

        public void Start(string coffeeType)
        {
            if (string.IsNullOrWhiteSpace(coffeeType))
                return;

            switch (coffeeType)
            {
                case CoffeeTypes.Mokka:
                    _mainController.MakeMokka();
                    break;
                case CoffeeTypes.Espresso:
                    _mainController.MakeEspresso();
                    break;
                case CoffeeTypes.Latte:
                    _mainController.MakeLatte();
                    break;
                case CoffeeTypes.Cappuccino:
                    _mainController.MakeCappuccino();
                    break;
                default:
                    throw new Exception("Selected coffee is invalid!");
            }

            // Reset
            _modelView.Reset();
        }
    }
}
