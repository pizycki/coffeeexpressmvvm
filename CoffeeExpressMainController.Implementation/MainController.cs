﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using CoffeeExpressMainController.Contract;
using CoffeeExpressControlPanel.Contract;

using Lab6.Display.Contract;

namespace CoffeeExpressMainController.Implementation
{
    /// <summary>
    /// This class stands for "making" coffee.
    /// Results are sent to provided display.
    /// </summary>
    public class MainController : IMainController
    {
        #region Fields

        IDisplay _display;

        #endregion

        /// <summary>
        /// Provided display.
        /// </summary>
        public IDisplay Display
        {
            get { return _display; }
            set { _display = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="display">Display</param>
        public MainController(IDisplay display)
        {
            _display = display;
        }

        #region IMainController members

        public void MakeEspresso()
        {
            PrepareCoffee(CoffeeTypes.Espresso);
        }

        public void MakeLatte()
        {
            PrepareCoffee(CoffeeTypes.Latte);
        }

        public void MakeMokka()
        {
            PrepareCoffee(CoffeeTypes.Mokka);
        }

        public void MakeCappuccino()
        {
            PrepareCoffee(CoffeeTypes.Cappuccino);
        }

        #endregion

        /// <summary>
        /// This method simulates work of coffee esspress.
        /// Let's say that it's super fast and prepares coffee instantly!
        /// </summary>
        private void PrepareCoffee(string coffeeType)
        {
            _display.Text = string.Format("Kawa {0} gotowa do odbioru!", coffeeType);
        }

        public static class CoffeeTypes
        {
            public const string Mokka = "Mokka";
            public const string Espresso = "Espresso";
            public const string Cappuccino = "Cappuccino";
            public const string Latte = "Latte";
        }
    }
}
