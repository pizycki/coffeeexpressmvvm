﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.Display.Contract;
namespace CoffeeExpressMainController.Contract
{
    public interface IMainController
    {
        IDisplay Display { get; set; }

        void MakeEspresso();
        void MakeLatte();
        void MakeMokka();
        void MakeCappuccino();
    }
}
