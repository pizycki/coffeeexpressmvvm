﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using Infrastructure;
using CoffeeExpressControlPanel.Contract;
using CoffeeExpressMainController.Contract;
using Lab6.Display.Contract;

using Ninject;

namespace Main
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        IDisplay _display;
        ICoffeeExpressControlPanel _controlPanel;
        IMainController _mainController;

        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            IKernel container = Configuration.RegisterComponents();

            _display = container.Get<IDisplay>();
            _display.Window.Show();
            _display.Text = "Test";

            _controlPanel = container.Get<ICoffeeExpressControlPanel>();
            _controlPanel.ControlPanelWindow.Show();

            _mainController = container.Get<IMainController>();
        }
    }
}
