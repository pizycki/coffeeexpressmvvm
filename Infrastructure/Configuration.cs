﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;

using CoffeeExpressMainController.Contract;
using CoffeeExpressMainController.Implementation;

using CoffeeExpressControlPanel.Contract;
using CoffeeExpressControlPanel.Implementation;

using Lab6.Display.Contract;
using Lab6.Display.Implementation;

namespace Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Creates container and registers all components used in application.
        /// </summary>
        /// <returns>Container.</returns>
        public static IKernel RegisterComponents()
        {
            IKernel container = new StandardKernel();

            // Bind main controller as singleton
            container.Bind<IMainController>().To<MainController>().InSingletonScope();

            // Bind control panel as singleton
            container.Bind<ICoffeeExpressControlPanel>().To<ControlPanel>().InSingletonScope();

            // Bind display as singleton
            container.Bind<IDisplay>().To<Display>().InSingletonScope();

            return container;
        }
    }
}
